#!/bin/bash
# Copyright (C) 2018, Codethink, Ltd., Robert Marshall <robert.marshall@codethink.co.uk>
# SPDX-License-Identifier:	AGPL-3.0
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, version 3.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.

set -e
#
# kernelci
#
git clone git://github.com/lucj/kernelci-docker

cd kernelci-docker
# assumes we want (and have) eth0
IP=$(ip addr show eth0 | grep "inet\b" | awk '{print $2}' | cut -d/ -f1)
# check if already in swarm mode?
docker swarm init --advertise-addr $IP
echo setting up kernelci
./start.sh
# retrieve the key from the output of the above? - though it is in .kernelci_token!
MASTER_KEY=`cat ./.kernelci_token`
cd ..
# cd  somewhere more generic?
git clone https://gitlab.com/cip-project/cip-testing/kernelci-build
sed -i kernelci-build/build.py -e "s/^token = None/token = \"${MASTER_KEY}\"/"
#
# initramfs
#
mkdir -p work/initramfs
ROOTPWD=`pwd`
cd work
# for init-example
git clone  https://gitlab.com/cip-project/cip-testing/board-at-desk-single-dev
# busybox
wget http://busybox.net/downloads/busybox-1.28.0.tar.bz2
tar -xf busybox-1.28.0.tar.bz2
cd busybox-1.28.0
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- defconfig
# sed config to STATIC
sed -i .config -e 's/# CONFIG_STATIC is not set/CONFIG_STATIC=y/'
# 
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- CONFIG_PREFIX=$ROOTPWD/work/initramfs $ROOTPWD/work/initramfs oldconfig
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- CONFIG_PREFIX=$ROOTPWD/work/initramfs $ROOTPWD/work/initramfs install
cd ../initramfs
# create /dev and special files
mkdir dev

# sudo needed!
sudo mknod dev/console c 5 1
sudo mknod dev/null c 1 3
sudo mknod dev/zero c 1 5

# populate /lib
mkdir lib usr/lib

rsync -a /usr/arm-linux-gnueabihf/lib/ ./lib/

# add mount points
mkdir proc sys root
# /etc and configuration files
mkdir etc

echo "null::sysinit:/bin/mount -a" > etc/inittab
echo "null::sysinit:/bin/hostname -F /etc/hostname" >> etc/inittab
echo "null::respawn:/bin/cttyhack /bin/login root" >> etc/inittab
echo "null::restart:/sbin/reboot" >> etc/inittab

echo "proc  /proc proc  defaults  0 0" > etc/fstab
echo "sysfs /sys  sysfs defaults  0 0" >> etc/fstab

echo  beagleboneblack > etc/hostname

echo "root::0:0:root:/root:/bin/sh" > etc/passwd

# use an example init from b@d
# 
cp ../board-at-desk-single-dev/scripts/init-example init

# create the initramfs
find . -depth -print | cpio -ocvB | gzip -c > ../initramfs.cpio.gz
echo and copy to docker!
CONT=`docker ps -qf "name=kernelci_proxy.1"`
docker cp ../initramfs.cpio.gz $CONT:/usr/share/nginx/html/kernelci/logs/
#
# set up lava
#
git clone git://github.com/kernelci/lava-docker
cd lava-docker
echo "  - name: beagleboneblack-01" >> boards.yaml
echo "    type: beaglebone-black"  >> boards.yaml
# IP address
echo "    connection_command: connectBBB.sh $IP 8020 root lavauser123"  >> boards.yaml
# to lava-master Dockerfile
# COPY ../../bbb.yaml /etc/lava-server/dispatcher-config/health-checks/beaglebone-black.yaml
# to lava-slave DockerFile
# RUN apt-get -y install expect
# COPY connectBBB.sh /usr/bin (from whereever...)

echo setting up lava-docker
./lavalab-gen.py
cd output/local
# this is where for me the docker network issues start to happen!
docker-compose build
docker-compose up -d


# need a down script which does
# cd lava-docker; docker-compose down
# cd kernelci-docker; stop.sh
# docker swarm leave --force
